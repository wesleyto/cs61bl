import java.awt.*;
import java.net.URL;
import java.util.ArrayList;

/**
 * A class that represents a picture. This class inherits from SimplePicture and
 * allows the student to add functionality and picture effects.
 * 
 * @author Barb Ericson (ericson@cc.gatech.edu) (Copyright Georgia Institute of
 *         Technology 2004)
 * @author Modified by Colleen Lewis (colleenl@berkeley.edu), Jonathan Kotker
 *         (jo_ko_berkeley@berkeley.edu), Kaushik Iyer (kiyer@berkeley.edu),
 *         George Wang (georgewang@berkeley.edu), and David Zeng
 *         (davidzeng@berkeley.edu), for use in CS61BL, the data structures
 *         course at University of California, Berkeley.
 */
public class Picture extends SimplePicture {

	// ///////////////////////// Static Variables //////////////////////////////

	// Different axes available to flip a picture.
	public static final int HORIZONTAL = 1;
	public static final int VERTICAL = 2;
	public static final int FORWARD_DIAGONAL = 3;
	public static final int BACKWARD_DIAGONAL = 4;

	// Different Picture objects for the bitmaps used in ASCII art conversion.
	private static Picture BMP_AMPERSAND;
	private static Picture BMP_APOSTROPHE;
	private static Picture BMP_AT;
	private static Picture BMP_BAR;
	private static Picture BMP_COLON;
	private static Picture BMP_DOLLAR;
	private static Picture BMP_DOT;
	private static Picture BMP_EXCLAMATION;
	private static Picture BMP_GRAVE;
	private static Picture BMP_HASH;
	private static Picture BMP_PERCENT;
	private static Picture BMP_SEMICOLON;
	private static Picture BMP_SPACE;

	// ////////////////////////// Constructors /////////////////////////////////

	/**
	 * A constructor that takes no arguments.
	 */
	public Picture() {
		super();
	}

	/**
	 * Creates a Picture from the file name provided.
	 * 
	 * @param fileName
	 *            The name of the file to create the picture from.
	 */
	public Picture(String fileName) {
		// Let the parent class handle this fileName.
		super(fileName);
	}

	/**
	 * Creates a Picture from the width and height provided.
	 * 
	 * @param width
	 *            the width of the desired picture.
	 * @param height
	 *            the height of the desired picture.
	 */
	public Picture(int width, int height) {
		// Let the parent class handle this width and height.
		super(width, height);
	}

	/**
	 * Creates a copy of the Picture provided.
	 * 
	 * @param pictureToCopy
	 *            Picture to be copied.
	 */
	public Picture(Picture pictureToCopy) {
		// Let the parent class do the copying.
		super(pictureToCopy);
	}

	/**
	 * Creates a copy of the SimplePicture provided.
	 * 
	 * @param pictureToCopy
	 *            SimplePicture to be copied.
	 */
	public Picture(SimplePicture pictureToCopy) {
		// Let the parent class do the copying.
		super(pictureToCopy);
	}

	// ///////////////////////////// Methods ///////////////////////////////////

	/**
	 * @return A string with information about the picture, such as filename,
	 *         height, and width.
	 */
	public String toString() {
		String output = "Picture, filename = " + this.getFileName() + ","
				+ " height = " + this.getHeight() + ", width = "
				+ this.getWidth();
		return output;
	}

	// ///////////////////// PROJECT 1 BEGINS HERE /////////////////////////////

	/*
	 * Each of the methods below is constructive: in other words, each of the
	 * methods below generates a new Picture, without permanently modifying the
	 * original Picture.
	 */

	// ////////////////////////////// Level 1 //////////////////////////////////
	/**
	 * Converts the Picture into grayscale. Since any variation of gray is
	 * obtained by setting the red, green, and blue components to the same
	 * value, a Picture can be converted into its grayscale component by setting
	 * the red, green, and blue components of each pixel in the new picture to
	 * the same value: the average of the red, green, and blue components of the
	 * same pixel in the original.
	 * 
	 * @return A new Picture that is the grayscale version of this Picture.
	 */
	public Picture grayscale() {
		Picture newPicture = new Picture(this);

		int pictureHeight = this.getHeight();
		int pictureWidth = this.getWidth();

		for (int x = 0; x < pictureWidth; x++) {
			for (int y = 0; y < pictureHeight; y++) {
				newPicture.setPixelToGray(x, y);
			}
		}
		return newPicture;
	}

	/**
	 * Helper method for grayscale() to set a pixel at (x, y) to be gray.
	 * 
	 * @param x
	 *            The x-coordinate of the pixel to be set to gray.
	 * @param y
	 *            The y-coordinate of the pixel to be set to gray.
	 */
	private void setPixelToGray(int x, int y) {
		Pixel currentPixel = this.getPixel(x, y);
		int average = currentPixel.getAverage();
		currentPixel.setRed(average);
		currentPixel.setGreen(average);
		currentPixel.setBlue(average);
	}

	/**
	 * Test method for setPixelToGray. This method is called by the JUnit file
	 * through the public method Picture.helpersWork().
	 */
	private static boolean setPixelToGrayWorks() {
		Picture bg = Picture.loadPicture("Creek.bmp");
		Pixel focalPixel = bg.getPixel(10, 10);
		bg.setPixelToGray(10, 10);
		int goalColor = (int) focalPixel.getAverage();
		int originalAlpha = focalPixel.getColor().getAlpha();
		boolean redCorrect = focalPixel.getRed() == goalColor;
		boolean greenCorrect = focalPixel.getGreen() == goalColor;
		boolean blueCorrect = focalPixel.getBlue() == goalColor;
		boolean alphaCorrect = focalPixel.getAlpha() == originalAlpha;
		return redCorrect && greenCorrect && blueCorrect && alphaCorrect;
	}

	/**
	 * This method provide JUnit access to the testing methods written within
	 * Picture.java
	 */
	public static boolean helpersWork() {
		if (!Picture.setPixelToGrayWorks()) {
			return false;
		}

		// You could put other tests here..

		return true;
	}

	/**
	 * Converts the Picture into its photonegative version. The photonegative
	 * version of an image is obtained by setting each of the red, green, and
	 * blue components of every pixel to a value that is 255 minus their current
	 * values.
	 * 
	 * @return A new Picture that is the photonegative version of this Picture.
	 */
	// negated is the new picture to be returned
	// the two for-loops analyzes every pixel in a column, from the top-down,
	// before moving on to the next column to the right (if any)
	// the helper function inside the two for-loop is explained below.
	public Picture negate() {
		Picture negated = new Picture(this);
		int picWidth = this.getWidth();
		int picHeight = this.getHeight();
		for (int x = 0; x < picWidth; x++) {
			for (int y = 0; y < picHeight; y++) {
				negated.setPixelToNegative(x, y);
			}
		}
		return negated;
	}

	// Helper function for negate method.
	// This method gets the color of the current pixel at x, y,
	// subtracts those color values from 255,
	// then sets the color of the pixel to those new calculated values.
	private void setPixelToNegative(int x, int y) {
		Pixel currentPixel = this.getPixel(x, y);
		int r_color = 255 - currentPixel.getRed();
		int g_color = 255 - currentPixel.getGreen();
		int b_color = 255 - currentPixel.getBlue();
		currentPixel.setRed(r_color);
		currentPixel.setGreen(g_color);
		currentPixel.setBlue(b_color);

	}
	// A checker method to validate x and y coordinates.
	private boolean isPixelOK(int x, int y) {
		if (0 <= x && 
			x <= this.getWidth() && 
			0 <= y && 
			y <= this.getHeight()) {
				return true;
		} else {
			throw new IllegalArgumentException("The X and/or Y coordinates given are out of bounds.");
		}
	}

	/**
	 * Creates an image that is lighter than the original image. The range of
	 * each color component should be between 0 and 255 in the new image. The
	 * alpha value should not be changed.
	 * 
	 * @return A new Picture that has every color value of the Picture increased
	 *         by the lightenAmount.
	 */
	// lightened is the new picture to be returned
	// the helper function inside the two for-loops analyzes every pixel and
	// sets it's color to something lighter,
	// as specified by the lightenAmount parameter.
	public Picture lighten(int lightenAmount) {
		Picture lightened = new Picture(this);
		int picwidth = this.getWidth();
		int picHeight = this.getHeight();
		for (int x = 0; x < picwidth; x++) {
			for (int y = 0; y < picHeight; y++) {
				lightened.setPixelLighter(x, y, lightenAmount);
			}
		}
		return lightened;

	}

	// Helper function for lighten() method
	// Lightened the color by getting the current pixel color values,
	// adding the specified amount to them,
	// then setting the pixel color to the new values.
	public void setPixelLighter(int x, int y, int amount) {
		Pixel currentPixel = this.getPixel(x, y);
		int r_color = currentPixel.getRed();
		int g_color = currentPixel.getGreen();
		int b_color = currentPixel.getBlue();
		currentPixel.setRed(r_color + amount);
		currentPixel.setGreen(g_color + amount);
		currentPixel.setBlue(b_color + amount);

	}

	/**
	 * Creates an image that is darker than the original image.The range of each
	 * color component should be between 0 and 255 in the new image. The alpha
	 * value should not be changed.
	 * 
	 * @return A new Picture that has every color value of the Picture decreased
	 *         by the darkenenAmount.
	 */

	// darkened is the new picture to be returned
	// the helper function inside the two for-loops analyzes every pixel and
	// sets it's color to something darker,
	// as specified by the darkenenAmount parameter.
	public Picture darken(int darkenenAmount) {
		Picture darkened = new Picture(this);
		int picwidth = this.getWidth();
		int picHeight = this.getHeight();
		for (int x = 0; x < picwidth; x++) {
			for (int y = 0; y < picHeight; y++) {
				darkened.setPixelDarker(x, y, darkenenAmount);
			}
		}
		return darkened;

	}

	// Helper function for darken() method
	// gets the value for each color in the pixel, then subtracts the amount
	// from that value, and sets the pixel to that new value.
	// the alpha value is untouched
	private void setPixelDarker(int x, int y, int amount) {
		Pixel currentPixel = this.getPixel(x, y);
		int r_color = currentPixel.getRed();
		int g_color = currentPixel.getGreen();
		int b_color = currentPixel.getBlue();
		currentPixel.setRed(r_color - amount);
		currentPixel.setGreen(g_color - amount);
		currentPixel.setBlue(b_color - amount);
	}

	/**
	 * Creates an image where the blue value has been increased by amount.The
	 * range of each color component should be between 0 and 255 in the new
	 * image. The alpha value should not be changed.
	 * 
	 * @return A new Picture that has every blue value of the Picture increased
	 *         by amount.
	 */
	public Picture addBlue(int amount) {
		Picture newPicture = new Picture(this);
		int pictureHeight = this.getHeight();
		int pictureWidth = this.getWidth();

		for (int x = 0; x < pictureWidth; x++) {
			for (int y = 0; y < pictureHeight; y++) {
				newPicture.increasePixelBlue(x, y, amount);
			}
		}
		return newPicture;
	}

	// increases pixel's blue value by amount
	// returns nothing
	// x and y represent pixel coordinates
	// doesn't matter if amount > 255 or < 0 because setBlue calls correctValue
	// which returns 0 or 255 if out of range.
	private void increasePixelBlue(int x, int y, int amount) {
		Pixel currentPixel = this.getPixel(x, y);
		currentPixel.setBlue(amount + currentPixel.getBlue());
	}

	/**
	 * Creates an image where the red value has been increased by amount. The
	 * range of each color component should be between 0 and 255 in the new
	 * image. The alpha value should not be changed.
	 * 
	 * @return A new Picture that has every red value of the Picture increased
	 *         by amount.
	 */
	public Picture addRed(int amount) {
		Picture newPicture = new Picture(this);
		int picHeight = this.getHeight();
		int picWidth = this.getWidth();

		for (int x = 0; x < picWidth; x++) {
			for (int y = 0; y < picHeight; y++) {
				newPicture.increasePixelRed(x, y, amount);
			}
		}
		return newPicture;
	}

	// addRed helper method sets all pixels to current red value amount
	// x and y represent pixel coordinates amount is the color increase
	// do not need to check if amount is out of range, setRed calls correctValue
	private void increasePixelRed(int x, int y, int amount) {
		Pixel currentPixel = this.getPixel(x, y);
		currentPixel.setRed(currentPixel.getRed() + amount);
	}

	/**
	 * Creates an image where the green value has been increased by amount. The
	 * range of each color component should be between 0 and 255 in the new
	 * image. The alpha value should not be changed.
	 * 
	 * @return A new Picture that has every green value of the Picture increased
	 *         by amount.
	 */
	public Picture addGreen(int amount) {
		Picture newPicture = new Picture(this);
		int picHeight = this.getHeight();
		int picWidth = this.getWidth();

		for (int x = 0; x < picWidth; x++) {
			for (int y = 0; y < picHeight; y++) {
				newPicture.increasePixelGreen(x, y, amount);
			}
		}
		return newPicture;
	}

	// helper method for addGreen sets all pixels current green values to its
	// current values + amount
	// x and y represent pixel coordinates
	// do not need to check value of amount because 
	// setGreen makes a call to correctValue.
	private void increasePixelGreen(int x, int y, int amount) {
		Pixel currentPixel = this.getPixel(x, y);
		currentPixel.setGreen(currentPixel.getGreen() + amount);
	}

	/**
	 * @param x
	 *            x-coordinate of the pixel currently selected.
	 * @param y
	 *            y-coordinate of the pixel currently selected.
	 * @param background
	 *            Picture to use as the background.
	 * @param threshold
	 *            Threshold within which to replace pixels.
	 * 
	 * @return A new Picture where all the pixels in the original Picture, which
	 *         differ from the currently selected pixel within the provided
	 *         threshold (in terms of color distance), are replaced with the
	 *         corresponding pixels in the background picture provided.
	 * 
	 *         If the two Pictures are of different dimensions, the new Picture
	 *         will have length equal to the smallest of the two Pictures being
	 *         combined, and height equal to the smallest of the two Pictures
	 *         being combined. In this case, the Pictures are combined as if
	 *         they were aligned at the top left corner (0, 0).
	 */
	// Creates a new photo, replaced, that has width and height set to the
	// minimums of the original photo and the background photo.
	// Compares the distance between comparisonColor and reference color.
	// If that distance is beyond the threshold, the corresponding pixel's color
	// is changed to the backgroundColor.
	// Else, the pixel color is copied over from the original photo.
	public Picture chromaKey(int xRef, int yRef, Picture background, int threshold) {
		this.isPixelOK(xRef, yRef);
		int x1 = background.getWidth();
		int y1 = background.getHeight();
		int x2 = this.getWidth();
		int y2 = this.getHeight();
		//height and width are the minimum values of both photos
		Picture replaced = new Picture(Math.min(x1, x2), Math.min(y1, y2));
		int picWidth = replaced.getWidth();
		int picHeight = replaced.getHeight();
		
		// referenceColor - variable that refers to the color of the reference
		// pixel in the original photo that is used for all comparisons.
		// comparisonColor - variable that refers to the color of the pixel in
		// the original photo that is being considered for replacement.
		// backgroundColor - variable that refers to the color of the pixel in
		// the background photo that may be used for replacement.
		for (int x = 0; x < picWidth; x++) {
			for (int y = 0; y < picHeight; y++) {
				Color referenceColor = this.getPixel(xRef, yRef).getColor();
				Color comparisonColor = this.getPixel(x, y).getColor();
				Color backgroundColor = background.getPixel(x, y).getColor();
				if (Pixel.colorDistance(comparisonColor, referenceColor) < threshold) {
					replaced.getPixel(x, y).setColor(backgroundColor);
				} else {
					replaced.getPixel(x, y).setColor(comparisonColor);
				}
			}
		}
		return replaced;
	}

	// ////////////////////////////// Level 2 //////////////////////////////////

	/**
	 * Rotates this Picture by the integer multiple of 90 degrees provided. If
	 * the number of rotations provided is positive, then the picture is rotated
	 * clockwise; else, the picture is rotated counterclockwise. Multiples of
	 * four rotations (including zero) correspond to no rotation at all.
	 * 
	 * @param rotations
	 *            The number of 90-degree rotations to rotate this image by.
	 * 
	 * @return A new Picture that is the rotated version of this Picture.
	 */
	public Picture rotate(int rotations) {
		int picHeight = this.getHeight();
		int picWidth = this.getWidth();
		Picture newPicture = new Picture(picHeight, picWidth);

		int correctR = rotations % 4; // changes CC to correct clockwise
		// rotations
		if (correctR > 0) {
			newPicture.rotateHelper(this);
			return newPicture.rotate(correctR - 1);
		} else {
			return this;
		}
	}

	// rotate helper function that rotates the picture clockwise once
	// origPicture is the picture that called rotate
	// returns nothing.
	private void rotateHelper(Picture origPicture) {

		int picWidth = this.getWidth();
		int picHeight = this.getHeight();

		for (int x = 0; x < picWidth; x++) {
			for (int y = 0; y < picHeight; y++) {
				Pixel changePixel = this.getPixel(x, y);
				Pixel origPixel = origPicture.getPixel(y, origPicture
						.getHeight()
						- 1 - x);
				changePixel.setColor(origPixel.getColor());
			}
		}
	}

	/**
	 * Flips this Picture about the given axis. The axis can be one of four
	 * static integer constants:
	 * 
	 * (a) Picture.HORIZONTAL: The picture should be flipped about a horizontal
	 * axis passing through the center of the picture. (b) Picture.VERTICAL: The
	 * picture should be flipped about a vertical axis passing through the
	 * center of the picture. (c) Picture.FORWARD_DIAGONAL: The picture should
	 * be flipped about an axis that passes through the north-east and
	 * south-west corners of the picture. (d) Picture.BACKWARD_DIAGONAL: The
	 * picture should be flipped about an axis that passes through the
	 * north-west and south-east corners of the picture.
	 * 
	 * @param axis
	 *            Axis about which to flip the Picture provided.
	 * 
	 * @return A new Picture flipped about the axis provided.
	 */
	public Picture flip(int axis) {
		Picture newPicture = new Picture(this);
		int picHeight = this.getHeight();
		int picWidth = this.getWidth();

		if (axis == HORIZONTAL) {
			for (int x = 0; x < picWidth; x++) {
				for (int y = 0; y < picHeight; y++) {
					newPicture.pixelSwapper(x, y, this);
				}
			}
		} else if (axis == VERTICAL) {
			newPicture = this.flip(HORIZONTAL).rotate(2);
		} else if (axis == FORWARD_DIAGONAL) {
			newPicture = this.rotate(1).flip(HORIZONTAL);
		} else if (axis == BACKWARD_DIAGONAL) {
			newPicture = this.rotate(1).flip(VERTICAL);
		}
		return newPicture;
	}

	// helper method for flip, changes newPicutre pixel at opposite y coordinate
	// to origPicture pixel and current x y coordinate.
	// x and y represent origPicture Pixel coordinates.
	// origPicture represents the picture that called flip
	// returns nothing
	private void pixelSwapper(int x, int y, Picture origPicture) {
		int picHeight = origPicture.getHeight();
		Pixel pixelToChange = this.getPixel(x, picHeight - 1 - y);
		Pixel origPixel = origPicture.getPixel(x, y);
		pixelToChange.setColor(origPixel.getColor());
	}

	/**
	 * @param threshold
	 *            Threshold to use to determine the presence of edges.
	 * 
	 * @return A new Picture that contains only the edges of this Picture. For
	 *         each pixel, we separately consider the color distance between
	 *         that pixel and the one pixel to its left, and also the color
	 *         distance between that pixel and the one pixel to the north, where
	 *         applicable. As an example, we would compare the pixel at (3, 4)
	 *         with the pixels at (3, 3) and the pixels at (2, 4). Also, since
	 *         the pixel at (0, 4) only has a pixel to its north, we would only
	 *         compare it to that pixel. If either of the color distances is
	 *         larger than the provided color threshold, it is set to black
	 *         (with an alpha of 255); otherwise, the pixel is set to white
	 *         (with an alpha of 255). The pixel at (0, 0) will always be set to
	 *         white.
	 */
	// edgy is a new picture to be returned that has the same width and height
	// as the original file
	// This method checks the case where both x and y are 0,
	// then checks for when one is 0, but not both,
	// then checks the last cases where neither are zero.
	// The calculations for the colorDistance are cast to (int) to prevent
	// comparison errors.
	public Picture showEdges(int threshold) {
		Picture edgy = new Picture(this);
		int picwidth = this.getWidth();
		int picHeight = this.getHeight();
		for (int x = 0; x < picwidth; x++) {
			for (int y = 0; y < picHeight; y++) {
				Color currentColor = this.getPixel(x, y).getColor();
				if (x == 0 && y == 0) { // if at 0, 0
					edgy.setEdge(x, y, false); // set to white
				} else if (x == 0) { // if on left edge, compare to north pixel
					Color northColor = this.getPixel(x, y - 1).getColor();
					boolean pred = ((int) Pixel.colorDistance(currentColor,
							northColor) > threshold);
					edgy.setEdge(x, y, pred);
				} else if (y == 0) { // if on top edge, compare to left pixel
					Color leftColor = this.getPixel(x - 1, y).getColor();
					boolean pred = ((int) Pixel.colorDistance(currentColor,
							leftColor) > threshold);
					edgy.setEdge(x, y, pred);
				} else { // if on neither edge, compare to both north and left.
					Color leftColor = this.getPixel(x - 1, y).getColor();
					Color northColor = this.getPixel(x, y - 1).getColor();
					boolean pred = ((int) Pixel.colorDistance(currentColor,
							northColor) > threshold);
					boolean pred2 = ((int) Pixel.colorDistance(currentColor,
							leftColor) > threshold);
					// or operator - if either predicate is true, the pixel is set to black.
					edgy.setEdge(x, y, (pred || pred2)); 
				}
			}
		}
		return edgy;
	}

	// helper for showEdges() method
	// if the boolean predicate is true, set the pixel color to black. Else,
	// white. Uses the default values for black and white from the Java API - Color module.
	// white and black both have alphas of 255.
	private void setEdge(int x, int y, boolean predicate) {
		if (predicate) {
			this.getPixel(x, y).setColor(Color.black);
		} else {
			this.getPixel(x, y).setColor(Color.white);
		}
	}

	// ////////////////////////////// Level 3 //////////////////////////////////

	/**
	 * @return A new Picture that is the ASCII art version of this Picture. To
	 *         implement this, the Picture is first converted into its grayscale
	 *         equivalent. Then, starting from the top left, the average color
	 *         of every chunk of 10 pixels wide by 20 pixels tall is computed.
	 *         Based on the average value obtained, this chunk will be replaced
	 *         by the corresponding ASCII character specified by the table
	 *         below.
	 * 
	 *         The ASCII characters to be used are available as Picture objects,
	 *         also of size 10 pixels by 20 pixels. The following characters
	 *         should be used, depending on the average value obtained:
	 * 
	 *         0 to 18: # (Picture.BMP_POUND) 19 to 37: @ (Picture.BMP_AT) 38 to
	 *         56: & (Picture.BMP_AMPERSAND) 57 to 75: $ (Picture.BMP_DOLLAR) 76
	 *         to 94: % (Picture.BMP_PERCENT) 95 to 113: | (Picture.BMP_BAR) 114
	 *         to 132: ! (Picture.BMP_EXCLAMATION) 133 to 151: ;
	 *         (Picture.BMP_SEMICOLON) 152 to 170: : (Picture.BMP_COLON) 171 to
	 *         189: ' (Picture.BMP_APOSTROPHE) 190 to 208: ` (Picture.BMP_GRAVE)
	 *         209 to 227: . (Picture.BMP_DOT) 228 to 255: (Picture.BMP_SPACE)
	 * 
	 *         We provide a getAsciiPic method to obtain the Picture object
	 *         corresponding to a character, given any of the static Strings
	 *         mentioned above.
	 * 
	 *         Note that the resultant Picture should be the exact same size as
	 *         the original Picture; this might involve characters being
	 *         partially copied to the final Picture.
	 */
	// gray is a new grayscale picture of the original picture
	// newPic is the picture to be returned
	// x_chunk marks the beginning x-coord of a chunk to be processed
	// y_chunk marks the beginning y-coord of a chunk to be processed
	public Picture convertToAscii() {
		Picture gray = this.grayscale();
		Picture newPic = new Picture(this);
		for (int x_chunk = 0; x_chunk < gray.getWidth(); x_chunk += 10) {
			for (int y_chunk = 0; y_chunk < gray.getHeight(); y_chunk += 20) {
				int chunkTotal = 0; // a running sum of the average colors of the pixels
				int count = 0; // a running sum of the number of pixels processed
				for (int x = 0; x < 10 && x + x_chunk < gray.getWidth(); x++) {
					for (int y = 0; y < 20 && y + y_chunk < gray.getHeight(); y++) {
						chunkTotal += gray.getPixel(x + x_chunk, y + y_chunk)
								.getAverage();
						count++;
					}
				}
				// the sum of the averages divided by the # of pixels
				int chunk_average = chunkTotal / count;
				// grabs the Ascii pic needed for the corresponding grayscale value of the chunk
				Picture replacePic = Picture.getAsciiPic(chunk_average);
				// x and y move within the borders of the chunk and of the
				// picture dimensions.
				// for loop replaces the current pixel with the colors from the
				// ASCII PIC.
				for (int x = 0; x < 10 && x + x_chunk < gray.getWidth(); x++) {
					for (int y = 0; y < 20 && y + y_chunk < gray.getHeight(); y++) {
						Color newColor = replacePic.getPixel(x, y).getColor();
						newPic.getPixel(x + x_chunk, y + y_chunk).setColor(
								newColor);
					}
				}
			}
		}
		return newPic;
	}

	/**
	 * Blurs this Picture. To achieve this, the algorithm takes a pixel, and
	 * sets it to the average value of all the pixels in a square of side (2 *
	 * blurThreshold) + 1, centered at that pixel. For example, if blurThreshold
	 * is 2, and the current pixel is at location (8, 10), then we will consider
	 * the pixels in a 5 by 5 square that has corners at pixels (6, 8), (10, 8),
	 * (6, 12), and (10, 12). If there are not enough pixels available -- if the
	 * pixel is at the edge, for example, or if the threshold is larger than the
	 * image -- then the missing pixels are ignored, and the average is taken
	 * only of the pixels available.
	 * 
	 * The red, blue, green and alpha values should each be averaged separately.
	 * 
	 * @param blurThreshold
	 *            Size of the blurring square around the pixel.
	 * 
	 * @return A new Picture that is the blurred version of this Picture, using
	 *         a blurring square of size (2 * threshold) + 1.
	 */
	public Picture blur(int blurThreshold) {
		Picture newPicture = new Picture(this);
		int picHeight = this.getHeight();
		int picWidth = this.getWidth();
		for (int x = 0; x < picWidth; x++) {
			for (int y = 0; y < picHeight; y++) {
				newPicture.blurHelper(x, y, blurThreshold, this);
			}
		}
		return newPicture;
	}

	// blurs a pixel at x, y coordinates using formula provided
	// x and y represent origPicture pixel coordinates
	// returns nothing
	private void blurHelper(int x, int y, int blurThreshold, Picture origPicture) {
		int redAverage, greenAverage, blueAverage, counter;
		redAverage = blueAverage = greenAverage = counter = 0;
		int picHeight = origPicture.getHeight();
		int picWidth = origPicture.getWidth();
		//averages the colors of the pixels within the blurThreshold box
		for (int j = x - blurThreshold; j <= x + blurThreshold; j++) {
			for (int i = y - blurThreshold; i <= y + blurThreshold; i++) {
				if (i >= 0 && j >= 0 && i < picHeight && j < picWidth) {
					redAverage += origPicture.getPixel(j, i).getRed();
					greenAverage += origPicture.getPixel(j, i).getGreen();
					blueAverage += origPicture.getPixel(j, i).getBlue();
					counter++;
				}
			}
		}
		this.getPixel(x, y).setRed(redAverage / counter);
		this.getPixel(x, y).setGreen(greenAverage / counter);
		this.getPixel(x, y).setBlue(blueAverage / counter);
	}

	/**
	 * @param x
	 *            x-coordinate of the pixel currently selected.
	 * @param y
	 *            y-coordinate of the pixel currently selected.
	 * @param threshold
	 *            Threshold within which to delete pixels.
	 * @param newColor
	 *            New color to color pixels.
	 * 
	 * @return A new Picture where all the pixels connected to the currently
	 *         selected pixel, and which differ from the selected pixel within
	 *         the provided threshold (in terms of color distance), are colored
	 *         with the new color provided.
	 */
	public Picture paintBucket(int x, int y, int threshold, Color newColor) {
		this.isPixelOK(x, y);
		Picture newPicture = new Picture(this);
		Color pixelColor = this.getPixel(x, y).getColor();
		ArrayList<Pixel> pixelArray = new ArrayList<Pixel>();
		pixelArray.add(newPicture.getPixel(x, y));
		newPicture.getPixel(x, y).setColor(newColor);
		// while the pixelArray is not empty send its x and y coordinates to
		// bucketHelper
		// then remove that pixel from the array.
		while (!pixelArray.isEmpty()) {
			Pixel currentPixel = pixelArray.get(0);
			int j = currentPixel.getX();
			int i = currentPixel.getY();
			newPicture.bucketHelper(j, i, threshold, pixelColor, newColor,
					pixelArray);
			pixelArray.remove(0);
		}

		return newPicture;
	}

	// helper method for paintBucket, paints the surrounding pixels at x, y
	// x and y represent coordinates of pixel passed in
	// pixelColor represents the reference pixel color
	// newColor is the color the pixels will be painted if within threshold
	// pixelArray represents Pixels that have been painted and needs its
	// surrounding pixels to be checked.
	// returns nothing.
	private void bucketHelper(int x, int y, int threshold, Color pixelColor, Color newColor, ArrayList<Pixel> pixelArray) {
		int picHeight = this.getHeight();
		int picWidth = this.getWidth();

		for (int j = x - 1; j <= x + 1 && j >= 0 && j < picWidth; j++) {
			for (int i = y - 1; i <= y + 1 && i >= 0 && i < picHeight; i++) {
				if (j != x || i != y) {//skips center pixel of the 3x3 box
					Pixel loopPixel = this.getPixel(j, i);
					if (loopPixel.colorDistance(pixelColor) < threshold
							&& loopPixel.getColor() != newColor) {
						loopPixel.setColor(newColor);
						pixelArray.add(loopPixel);
					}
				}
			}
		}

	}

	// /////////////////////// PROJECT 1 ENDS HERE /////////////////////////////
	public boolean equals(Object obj) {
		if (!(obj instanceof Picture)) {
			return false;
		}

		Picture p = (Picture) obj;
		// Check that the two pictures have the same dimensions.
		if ((p.getWidth() != this.getWidth())
				|| (p.getHeight() != this.getHeight())) {
			return false;
		}

		// Check each pixel.
		for (int x = 0; x < this.getWidth(); x++) {
			for (int y = 0; y < this.getHeight(); y++) {
				if (!this.getPixel(x, y).equals(p.getPixel(x, y))) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Helper method for loading a picture in the current directory.
	 */
	protected static Picture loadPicture(String pictureName) {
		URL url = Picture.class.getResource(pictureName);
		return new Picture(url.getFile().replaceAll("%20", " "));
	}

	/**
	 * Helper method for loading the pictures corresponding to each character
	 * for the ASCII art conversion.
	 */
	private static Picture getAsciiPic(int grayValue) {
		int asciiIndex = (int) grayValue / 19;

		if (BMP_AMPERSAND == null) {
			BMP_AMPERSAND = Picture.loadPicture("ampersand.bmp");
			BMP_APOSTROPHE = Picture.loadPicture("apostrophe.bmp");
			BMP_AT = Picture.loadPicture("at.bmp");
			BMP_BAR = Picture.loadPicture("bar.bmp");
			BMP_COLON = Picture.loadPicture("colon.bmp");
			BMP_DOLLAR = Picture.loadPicture("dollar.bmp");
			BMP_DOT = Picture.loadPicture("dot.bmp");
			BMP_EXCLAMATION = Picture.loadPicture("exclamation.bmp");
			BMP_GRAVE = Picture.loadPicture("grave.bmp");
			BMP_HASH = Picture.loadPicture("hash.bmp");
			BMP_PERCENT = Picture.loadPicture("percent.bmp");
			BMP_SEMICOLON = Picture.loadPicture("semicolon.bmp");
			BMP_SPACE = Picture.loadPicture("space.bmp");
		}

		switch (asciiIndex) {
		case 0:
			return Picture.BMP_HASH;
		case 1:
			return Picture.BMP_AT;
		case 2:
			return Picture.BMP_AMPERSAND;
		case 3:
			return Picture.BMP_DOLLAR;
		case 4:
			return Picture.BMP_PERCENT;
		case 5:
			return Picture.BMP_BAR;
		case 6:
			return Picture.BMP_EXCLAMATION;
		case 7:
			return Picture.BMP_SEMICOLON;
		case 8:
			return Picture.BMP_COLON;
		case 9:
			return Picture.BMP_APOSTROPHE;
		case 10:
			return Picture.BMP_GRAVE;
		case 11:
			return Picture.BMP_DOT;
		default:
			return Picture.BMP_SPACE;
		}
	}

	public static void main(String[] args) {
		Picture initialPicture = new Picture(FileChooser
				.pickAFile(FileChooser.OPEN));
		initialPicture.explore();
	}

} // End of Picture class