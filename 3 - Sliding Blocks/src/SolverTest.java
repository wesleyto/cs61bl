import junit.framework.TestCase;


public class SolverTest extends TestCase {
	
	
	int testNum = 0;
	
	// easy tests
	public void testBigBlock1() {
        System.out.println("big.block.1");
        System.out.println();
		Solver s = new Solver("easy/big.block.1", "easy/big.block.1.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testBigSearch1() {
        System.out.println("big.search.1");
        System.out.println();
		Solver s = new Solver("easy/big.search.1", "easy/big.search.1.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testForcedMatched2() {
        System.out.println("forced.match.2");
        System.out.println();
		Solver s = new Solver("easy/forced.match.2", "easy/forced.match.2.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testImmedMatch0() {
        System.out.println("immed.match.0");
        System.out.println();
		Solver s = new Solver("easy/immed.match.0", "easy/immed.match.0.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testImmedMatch1() {
        System.out.println("immed.match.1");
        System.out.println();
		Solver s = new Solver("easy/immed.match.1", "easy/immed.match.1.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testImmedMatch2_180() {
        System.out.println("immed.match.2+180");
        System.out.println();
		Solver s = new Solver("easy/immed.match.2+180", "easy/immed.match.2+180.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testImmedMatch2_270() {
        System.out.println("immed.match.2+270");
        System.out.println();
		Solver s = new Solver("easy/immed.match.2+270", "easy/immed.match.2+270.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testSmallSearch() {
        System.out.println("small.search");
        System.out.println();
		Solver s = new Solver("easy/small.search", "easy/small.search.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testSmallSearch_90() {
        System.out.println("small.search+90");
        System.out.println();
		Solver s = new Solver("easy/small.search+90", "easy/small.search+90.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testTree() {
        System.out.println("tree");
        System.out.println();
		Solver s = new Solver("easy/tree", "easy/tree.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testTree_90() {
        System.out.println("tree+90");
        System.out.println();
		Solver s = new Solver("easy/tree+90", "easy/tree+90.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	
	// medium tests
	public void testDads() {
        System.out.println("dads");
        System.out.println();
		Solver s = new Solver("medium/dads", "medium/dads.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testDads_180() {
        System.out.println("dads+180");
        System.out.println();
		Solver s = new Solver("medium/dads+180", "medium/dads+180.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testBlockado() {
        System.out.println("blockado");
        System.out.println();
		Solver s = new Solver("medium/blockado", "medium/blockado.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testC28() {
        System.out.println("c28");
        System.out.println();
		Solver s = new Solver("medium/c28", "medium/28.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testC29() {
        System.out.println("c29");
        System.out.println();
		Solver s = new Solver("medium/c29", "medium/29.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testC31() {
        System.out.println("c31");
        System.out.println();
		Solver s = new Solver("medium/c31", "medium/31.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testC32() {
        System.out.println("c32");
        System.out.println();
		Solver s = new Solver("medium/c32", "medium/32.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testC33() {
        System.out.println("c33");
        System.out.println();
		Solver s = new Solver("medium/c33", "medium/33.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testC34() {
        System.out.println("c34");
        System.out.println();
		Solver s = new Solver("medium/c34", "medium/34.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testC35() {
        System.out.println("c35");
        System.out.println();
		Solver s = new Solver("medium/c35", "medium/35.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testC36() {
        System.out.println("c36");
        System.out.println();
		Solver s = new Solver("medium/c36", "medium/36.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testC37() {
        System.out.println("c37");
        System.out.println();
		Solver s = new Solver("medium/c37", "medium/37.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testC38() {
        System.out.println("c38");
        System.out.println();
		Solver s = new Solver("medium/c38", "medium/38.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	
	// hard tests
	public void testSuperCentury() {
        System.out.println("super.century");
        System.out.println();
		Solver s = new Solver("hard/super.century", "hard/super.century.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}	
	public void testPennant() {
        System.out.println("pennant");
        System.out.println();
		Solver s = new Solver("hard/pennant", "hard/pennant.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testLaneRouge() {
        System.out.println("lane.rouge");
        System.out.println();
		Solver s = new Solver("hard/lane.rouge", "hard/lane.rouge.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testSuperCentury2() {
        System.out.println("super.century2");
        System.out.println();
		Solver s = new Solver("hard/super.century2", "hard/super.century2.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}
	public void testC22() {
        System.out.println("c22");
        System.out.println();
		Solver s = new Solver("hard/c22", "hard/22.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}	
	public void testC25() {
        System.out.println("c25");
        System.out.println();
		Solver s = new Solver("hard/c25", "hard/15.23-27.30.41.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}	
	public void testC26() {
        System.out.println("c26");
        System.out.println();
		Solver s = new Solver("hard/c26", "hard/15.23-27.30.41.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}	
	public void testC46() {
        System.out.println("c46");
        System.out.println();
		Solver s = new Solver("hard/c46", "hard/46.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}	
	public void testC53() {
        System.out.println("c53");
        System.out.println();
		Solver s = new Solver("hard/c53", "hard/53.54.64.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}	
	public void testC54() {
        System.out.println("c54");
        System.out.println();
		Solver s = new Solver("hard/c54", "hard/53.54.64.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}	
	public void testC55() {
        System.out.println("c55");
        System.out.println();
		Solver s = new Solver("hard/c55", "hard/55.56.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}	
	public void testPandemonium() {
        System.out.println("pandemonium");
        System.out.println();
		Solver s = new Solver("hard/pandemonium", "hard/pandemonium.goal");
		s.runGame(testNum);
        System.out.println();
        System.out.print("===============");
        System.out.println();
        System.out.println();
	}	

	// impossible tests (no solution)
	// Every one of these tests will eventually cause system exit.
	// Comment out when not in use.
	// Only run one at a time.
//	public void test140x140_imp() {
//		System.out.println("140x140 impossible");
//		System.out.println();
//		Solver s = new Solver("easy/140x140", "easy/140x140.impossible.goal");
//		System.out.println();
//		System.out.print("===============");
//		System.out.println();
//		System.out.println();
//	}
//	public void testBigTray4() {
//		Board b = new Board("hard/big.tray.4");
//		b.printTaken();
//		System.out.println("\n\n");
//		Board bc = new Board(b, "hard/many.blocks.20.goal");
//		bc.printTaken();
//		Solver s = new Solver("hard/big.tray.4", "hard/many.blocks.20.goal");
//		s.runGame(testNum);
//	}
//	public void testc71() {
//		Board b = new Board("hard/c71");
//		b.printTaken();
//		System.out.println("\n\n");
//		Board bc = new Board(b, "hard/71.goal");
//		bc.printTaken();
//		Solver s = new Solver("hard/c71", "hard/71.goal");
//	}
	
}
