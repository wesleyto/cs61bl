import java.util.*;

@SuppressWarnings("unchecked") 
// Collections.sort throws an "unchecked" warning 
// because it turns a List<Board> into a generic list.
// We are suppressing the warning in case it 
// messes up the autograder.

public class Solver {

	private Board board, goalBoard;
	private HashSet<Board> alreadySeen = new HashSet<Board>();
	private Stack<Board> boardsToCheck = new Stack<Board>();
	private PriorityQueue<BoardNode> queue = new PriorityQueue<BoardNode>();
	private static boolean boardIsOK = false;
	private static boolean hashSize = false;
	private static boolean printBoard = false;
	private static boolean queueSize = false;
	private static boolean printBlocks = false;
	private static boolean hashCode = false;
	private static boolean numberOfMoves = false;

	/**
	 * Constructs a pair of boards, the initial board, and the goal board.
	 * 
	 * @param initConfig
	 *            a file containing a board height and width, and the block
	 *            configurations for that board.
	 * @param goalConfig
	 *            a file containing the block configurations for a goal board.
	 * 
	 */
	public Solver(String initConfig, String goalConfig) {
		board = new Board(initConfig);
		goalBoard = new Board(board, goalConfig);

		// debugger option
		if (boardIsOK) {
			board.isOK();
			goalBoard.isOK();
		}

		// Solved if the initial configuration is the same as the goal
		// configuration
		if (board.checkBoard(goalBoard.blocks())) {
			System.exit(0);
		}

	}

	/**
	 * run the game using the boards constructed by Solver. The method used to
	 * solve the game is selected by an int.
	 * 
	 * @param choice
	 *            an int indicating the choice of method to use for the game. 0:
	 *            pickMethod 1: runGameSparse 2: runGameSparseN 3: runGameDense
	 *            4: runGameDenseSmart 5: runGameDenseSmart_PS 6:
	 *            runGameSparseNSmart 7: runGameSparseSmart_PS
	 * 
	 */
	public void runGame(int choice) {
		Board soln = null;

		boardsToCheck.push(board);
		switch (choice) {
		case 0:
			soln = pickMethod();
			break;
		case 1:
			soln = runGameSparse();
			break;
		case 2:
			soln = runGameSparseN();
			break;
		case 3:
			soln = runGameDense();
			break;
		case 4:
			soln = runGameDenseSmart();
			break;
		case 5:
			soln = runGameDenseSmart_PS();
			break;
		case 6:
			soln = runGameSparseNSmart();
			break;
		case 7:
			soln = runGameSparseSmart_PS();
			break;
		}

		printSolution(soln);

	}
	
	/**
	 * A looping algorithm that prints out the solution of a board.
	 * Begins at the soln board, calls the getMoves method, 
	 * pushes the moves onto a stack, and then calls the 
	 * getParent method, repeating until the parent board (the init
	 * board) is reached. The moves are then popped off the stack
	 * and printed one at a time.
	 * 
	 * @param soln
	 * 			a board representing the solution to the initial board.
	 */

	public void printSolution(Board soln) {

		if (soln == null) {
			System.exit(1);
		}
		// begins printing the solution path
		else {
			int counter = 0;// used by debugger
			Board curBoard = soln;
			Stack<String> path = new Stack<String>();
			while (curBoard != null) {
				path.push(curBoard.getMoves());
				curBoard = curBoard.getParentBoard();
			}
			while (!path.isEmpty()) {
				String move = path.pop();
				if (move != null) {
					System.out.println(move);
					counter++;
				}
			}
			if (numberOfMoves) // debugger option
			{
				System.out.println("number of moves made: " + counter);
			}
		}
	}
	
	/**
	 * Compares the number of empty spaces to the number of 
	 * occupied spaces, and selects a run method base on those 
	 * numbers. The cutoff point is 50%. If the number of occupied
	 * spaces is greater than or equal to the number of empty spaces
	 * on the board, rungamedenseSmart_PS is selected. Otherwise,
	 * rungameSparseSmart_PS is selected.
	 * 
	 * @return
	 */

	private Board pickMethod() {

		Board startBoard = board;

		boolean[][] taken = startBoard.buildBooleanTaken();
		int blockCount = 0, whiteCount = 0;
		for (int i = 0; i < startBoard.height(); i++) {
			for (int j = 0; j < startBoard.width(); j++) {
				if (taken[i][j])
					blockCount++;
				else
					whiteCount++;
			}
		}
		if (blockCount >= whiteCount) {
			return runGameDenseSmart_PS();
		} else {
			return runGameSparseSmart_PS();
		}
	}
	
	/**
	 * Generates moves of the sparse board. Returns the 
	 * solution once it finds it. Uses a stack. If the stack is
	 * empty (all moves have been checked), returns no 
	 * solution board (null).
	 * 
	 * @return
	 * 			the solution board, if found, or null if there is no solution.
	 */

	private Board runGameSparse() {

		int numBlocks = board.blocks().length;

		while (!boardsToCheck.isEmpty()) {
			Board cur = boardsToCheck.pop();
			// debugger methods
			if (printBoard) {
				System.out.println("Current Board is:");
				cur.printTaken();
			}
			if (printBlocks) {
				cur.printBlocks();
			}
			if (hashCode) {
				System.out.println("Current boards hash code is: "
						+ cur.hashCode());
			}
			if (hashSize) {
				System.out.println("Current hash set size is: "
						+ alreadySeen.size());
			}
			if (queueSize) {
				System.out.println("Current size of stack is: "
						+ boardsToCheck.size());
			}
			if (boardIsOK) {
				System.out.println("checking board");
				cur.isOK();
			}
			// end of debugger methods

			if (alreadySeen.contains(cur)) {
				continue;
			}
			if (cur.checkBoard(goalBoard.blocks())) {
				return cur;
			}
			alreadySeen.add(cur);

			for (int i = 0; i < numBlocks; i++) {
				if (cur.canMove(i, Board.Direction.UP)) {
					boardsToCheck.push(cur.move(i, Board.Direction.UP));
				}
				if (cur.canMove(i, Board.Direction.DOWN)) {
					boardsToCheck.push(cur.move(i, Board.Direction.DOWN));
				}
				if (cur.canMove(i, Board.Direction.LEFT)) {
					boardsToCheck.push(cur.move(i, Board.Direction.LEFT));
				}
				if (cur.canMove(i, Board.Direction.RIGHT)) {
					boardsToCheck.push(cur.move(i, Board.Direction.RIGHT));
				}
			}
		}
		return null;// found no solution
	}
	
	/**
	 * alternative method based on Sparse() to move more than one space 
	 * given a current board configuration.
	 * 
	 * @return
	 * 			the solution board, if found, or null if there is no solution.
	 */

	private Board runGameSparseN() {

		int numBlocks = board.blocks().length;

		while (!boardsToCheck.isEmpty()) {
			Board cur = boardsToCheck.pop();
			// debugger methods
			if (printBoard) {
				System.out.println("Current board is:");
				cur.printTaken();
			}
			if (printBlocks) {
				cur.printBlocks();
			}
			if (hashCode) {
				System.out.println("Current boards hash code is: "
						+ cur.hashCode());
			}
			if (hashSize) {
				System.out.println("Current hash set size is: "
						+ alreadySeen.size());
			}
			if (queueSize) {
				System.out.println("Current size of stack is: "
						+ boardsToCheck.size());
			}
			if (boardIsOK) {
				System.out.println("checking board");
				cur.isOK();
			}
			// end of debugger methods
			if (alreadySeen.contains(cur)) {
				continue;
			}
			if (cur.checkBoard(goalBoard.blocks())) {
				return cur;
			}
			alreadySeen.add(cur);

			for (int i = 0; i < numBlocks; i++) {
				if (cur.canMove(i, Board.Direction.UP)) {
					Board tempBoard = cur;
					while (tempBoard.canMove(i, Board.Direction.UP)) {
						boardsToCheck.push(tempBoard
								.move(i, Board.Direction.UP));
						tempBoard = tempBoard.move(i, Board.Direction.UP);
					}
				}
				if (cur.canMove(i, Board.Direction.DOWN)) {
					Board tempBoard = cur;
					while (tempBoard.canMove(i, Board.Direction.DOWN)) {
						boardsToCheck.push(tempBoard.move(i,
								Board.Direction.DOWN));
						tempBoard = tempBoard.move(i, Board.Direction.DOWN);
					}
				}
				if (cur.canMove(i, Board.Direction.LEFT)) {
					Board tempBoard = cur;
					while (tempBoard.canMove(i, Board.Direction.LEFT)) {
						boardsToCheck.push(tempBoard.move(i,
								Board.Direction.LEFT));
						tempBoard = tempBoard.move(i, Board.Direction.LEFT);
					}
				}
				if (cur.canMove(i, Board.Direction.RIGHT)) {
					Board tempBoard = cur;
					while (tempBoard.canMove(i, Board.Direction.RIGHT)) {
						boardsToCheck.push(tempBoard.move(i,
								Board.Direction.RIGHT));
						tempBoard = tempBoard.move(i, Board.Direction.RIGHT);
					}
				}
			}
		}
		return null;// found no solution
	}
	
	/**
	 * While the stack is not empty pop a board off the stack
	 * check to see if its the board solution, if not then
	 * generate all available one space moves given that current 
	 * board, and add them to the stack to be checked.
	 * If no solution is found return null.
	 * 
	 * @return
	 *  		the solution board, if found, or null if there is no solution.
	 */

	private Board runGameDense() {
		while (!boardsToCheck.isEmpty()) {
			Board b = boardsToCheck.pop();
			// debugger methods
			if (printBoard) {
				System.out.println("Current board is:");
				b.printTaken();
			}
			if (printBlocks) {
				b.printBlocks();
			}
			if (hashCode) {
				System.out.println("Current boards hash code is: "
						+ b.hashCode());
			}
			if (hashSize) {
				System.out.println("Current hash set size is: "
						+ alreadySeen.size());
			}
			if (queueSize) {
				System.out.println("Current size of stack is: "
						+ boardsToCheck.size());
			}
			// end of debugger methods

			if (alreadySeen.contains(b)) {
				continue;
			}
			if (b.checkBoard(goalBoard.blocks())) {
				return b;
			}

			alreadySeen.add(b);
			ArrayList<Board> possibleBoards = b.generateMoveList();
			// debugger option if enabled checks that all newly created boards
			// are valid
			if (boardIsOK) {
				for (Board brd : possibleBoards) {
					System.out.println("checking board");
					brd.isOK();
				}
			}
			// check to see if boards have already been visited

			// add to stack to be checked
			for (Board i : possibleBoards) {
				// if (!alreadySeen.contains(i)) {
				boardsToCheck.push(i);
				// }
			}
		}
		return null; // found no solution
	}
	
	/**
	 * Alternative version to runGameDense using a priority queue 
	 * instead of a stack.  
	 * 
	 * @return
	 *  		the solution board, if found, or null if there is no solution.
	 */

	public Board runGameDenseSmart() {
		Board startBoard = boardsToCheck.pop();
		queue.add(new BoardNode(startBoard, startBoard
				.getDistanceToGoal(goalBoard.blocks())));

		while (!queue.isEmpty()) {

			Board b = (queue.remove()).myBoard;
			// debugger methods
			if (printBoard) {
				System.out.println("Current board is:");
				b.printTaken();
			}
			if (printBlocks) {
				b.printBlocks();
			}
			if (hashCode) {
				System.out.println("Current boards hash code is: "
						+ b.hashCode());
			}
			if (hashSize) {
				System.out.println("Current hash set size is: "
						+ alreadySeen.size());
			}
			if (queueSize) {
				System.out.println("Current size of queue is: " + queue.size());
			}
			// end of debug methods

			if (b.checkBoard(goalBoard.blocks())) {
				return b;
			}

			ArrayList<Board> possibleBoards = b.generateMoveList();
			// debugger method
			if (boardIsOK) {
				for (Board i : possibleBoards) {
					System.out.println("checking board");
					i.isOK();
				}
			}

			// add to stack to be checked
			int count = 0;
			for (Board i : possibleBoards) {
				if (!alreadySeen.contains(i)) {
					queue.add(new BoardNode(i, i.getDistanceToGoal(goalBoard
							.blocks())));
					alreadySeen.add(i);
					count++;
				}

			}

		}
		return null;
	}
	
	/**
	 * Alternative version to runGameDenseSmart except it uses a
	 * priority stack instead of a priority queue.
	 * 
	 * @return
	 *  		the solution board, if found, or null if there is no solution.
	 */

	public Board runGameDenseSmart_PS() { // PS = "priority stack"
		Stack<BoardNode> priorityStack = new Stack<BoardNode>();
		Board startBoard = boardsToCheck.pop();
		priorityStack.push(new BoardNode(startBoard, startBoard
				.getDistanceToGoal(goalBoard.blocks())));

		while (!priorityStack.isEmpty()) {
			Board b = priorityStack.pop().myBoard;

			// debugger methods
			if (printBoard) {
				System.out.println("Current board is:");
				b.printTaken();
			}
			if (printBlocks) {
				b.printBlocks();
			}
			if (hashCode) {
				System.out.println("Current boards hash code is: "
						+ b.hashCode());
			}
			if (hashSize) {
				System.out.println("Current hash set size is: "
						+ alreadySeen.size());
			}
			if (queueSize) {
				System.out.println("Current size of priority stack is: "
						+ priorityStack.size());
			}
			// end of debug methods

			if (b.checkBoard(goalBoard.blocks())) {
				return b;
			}

			ArrayList<Board> possibleBoards = b.generateMoveList();
			// debugger method
			if (boardIsOK) {
				for (Board i : possibleBoards) {
					System.out.println("checking board");
					i.isOK();
				}
			}

			ArrayList<BoardNode> possibleBoardNodes = new ArrayList<BoardNode>();
			for (Board i : possibleBoards) {
				if (!alreadySeen.contains(i)) {
					possibleBoardNodes.add(new BoardNode(i, i
							.getDistanceToGoal(goalBoard.blocks())));
				}
			}
			Collections.sort(possibleBoardNodes); // sorts in ascending order of
			// distances
			Collections.reverse(possibleBoardNodes); // now in descending order
			// - highest distances
			// are first

			int count = 0;
			for (BoardNode i : possibleBoardNodes) {
				priorityStack.push(i);
				alreadySeen.add(i.myBoard);
				count++;
			}
		}
		return null;
	}
	
	/**
	 * Alternative version of runGameSparseN except instead
	 * of using a stack it uses a priority stack. 
	 * 
	 * @return
	 *  		the solution board, if found, or null if there is no solution.
	 */

	public Board runGameSparseNSmart() {
		Stack<BoardNode> priorityStack = new Stack<BoardNode>();
		Board startBoard = boardsToCheck.pop();
		priorityStack.push(new BoardNode(startBoard, startBoard
				.getDistanceToGoal(goalBoard.blocks())));

		while (!priorityStack.isEmpty()) {

			Board b = priorityStack.pop().myBoard;

			// debugger methods
			if (printBoard) {
				System.out.println("Current board is:");
				b.printTaken();
			}
			if (printBlocks) {
				b.printBlocks();
			}
			if (hashCode) {
				System.out.println("Current boards hash code is: "
						+ b.hashCode());
			}
			if (hashSize) {
				System.out.println("Current hash set size is: "
						+ alreadySeen.size());
			}
			if (queueSize) {
				System.out.println("Current size of priority stack is: "
						+ priorityStack.size());
			}
			// end of debug methods

			if (b.checkBoard(goalBoard.blocks())) {
				return b;
			}

			ArrayList<Board> possibleBoards = new ArrayList<Board>();
			int numBlocks = b.blocks().length;

			for (int i = 0; i < numBlocks; i++) {
				if (b.canMove(i, Board.Direction.UP)) {
					Board tempBoard = b;
					while (tempBoard.canMove(i, Board.Direction.UP)) {
						possibleBoards.add(tempBoard
								.move(i, Board.Direction.UP));
						tempBoard = tempBoard.move(i, Board.Direction.UP);
					}
				}
				if (b.canMove(i, Board.Direction.DOWN)) {
					Board tempBoard = b;
					while (tempBoard.canMove(i, Board.Direction.DOWN)) {
						possibleBoards.add(tempBoard.move(i,
								Board.Direction.DOWN));
						tempBoard = tempBoard.move(i, Board.Direction.DOWN);
					}
				}
				if (b.canMove(i, Board.Direction.LEFT)) {
					Board tempBoard = b;
					while (tempBoard.canMove(i, Board.Direction.LEFT)) {
						possibleBoards.add(tempBoard.move(i,
								Board.Direction.LEFT));
						tempBoard = tempBoard.move(i, Board.Direction.LEFT);
					}
				}
				if (b.canMove(i, Board.Direction.RIGHT)) {
					Board tempBoard = b;
					while (tempBoard.canMove(i, Board.Direction.RIGHT)) {
						possibleBoards.add(tempBoard.move(i,
								Board.Direction.RIGHT));
						tempBoard = tempBoard.move(i, Board.Direction.RIGHT);
					}
				}
			}

			// debugger method
			if (boardIsOK) {
				for (Board i : possibleBoards) {
					System.out.println("checking board");
					i.isOK();
				}
			}

			ArrayList<BoardNode> possibleBoardNodes = new ArrayList<BoardNode>();
			for (Board i : possibleBoards) {
				if (!alreadySeen.contains(i)) {
					possibleBoardNodes.add(new BoardNode(i, i
							.getDistanceToGoal(goalBoard.blocks())));
				}
			}
			Collections.sort(possibleBoardNodes); // sorts in ascending order of
			// distances
			Collections.reverse(possibleBoardNodes); // now in descending order
			// - highest distances
			// are first

			for (BoardNode i : possibleBoardNodes) {

				priorityStack.push(i);
				alreadySeen.add(i.myBoard);
			}

		}
		return null;
	}
	
	/**
	 * Alternative version to runGameSparseSmart except instead
	 * of using a priority queue it uses a priority stack.
	 * 
	 * @return
	 *  		the solution board, if found, or null if there is no solution.
	 */

	public Board runGameSparseSmart_PS() { // PS = "priority stack"
		Stack<BoardNode> priorityStack = new Stack<BoardNode>();
		Board startBoard = boardsToCheck.pop();
		priorityStack.push(new BoardNode(startBoard, startBoard
				.getDistanceToGoal(goalBoard.blocks())));

		while (!priorityStack.isEmpty()) { 
			Board b = priorityStack.pop().myBoard;
			// debugger methods
			if (printBoard) {
				System.out.println("Current board is:");
				b.printTaken();
			}
			if (printBlocks) {
				b.printBlocks();
			}
			if (hashCode) {
				System.out.println("Current boards hash code is: "
						+ b.hashCode());
			}
			if (hashSize) {
				System.out.println("Current hash set size is: "
						+ alreadySeen.size());
			}
			if (queueSize) {
				System.out.println("Current size of priority Stack is: "
						+ priorityStack.size());
			}
			// end of debug methods

			if (b.checkBoard(goalBoard.blocks())) {
				return b;
			}

			ArrayList<Board> possibleBoards = new ArrayList<Board>();
			int numBlocks = b.blocks().length;

			for (int i = 0; i < numBlocks; i++) {
				if (b.canMove(i, Board.Direction.UP)) {
					possibleBoards.add(b.move(i, Board.Direction.UP));
				}
				if (b.canMove(i, Board.Direction.DOWN)) {
					possibleBoards.add(b.move(i, Board.Direction.DOWN));
				}
				if (b.canMove(i, Board.Direction.LEFT)) {
					possibleBoards.add(b.move(i, Board.Direction.LEFT));
				}
				if (b.canMove(i, Board.Direction.RIGHT)) {
					possibleBoards.add(b.move(i, Board.Direction.RIGHT));
				}
			}
			// debugger method
			if (boardIsOK) {
				for (Board i : possibleBoards) {
					System.out.println("checking board");
					i.isOK();
				}
			}

			ArrayList<BoardNode> possibleBoardNodes = new ArrayList<BoardNode>();
			for (Board i : possibleBoards) {
				if (!alreadySeen.contains(i)) {
					possibleBoardNodes.add(new BoardNode(i, i
							.getDistanceToGoal(goalBoard.blocks())));
				}
			}
			Collections.sort(possibleBoardNodes); // sorts in ascending order of
			// distances
			Collections.reverse(possibleBoardNodes); // now in descending order
			// - highest distances
			// are first

			for (BoardNode i : possibleBoardNodes) {
				priorityStack.push(i);
				alreadySeen.add(i.myBoard);
			}
		}
		return null;
	}

	/**
	 * The debugger takes the first arg and gets the first two characters of the string
	 * if its equal to -o then enter the debugger, otherwise run Solver normally with the 
	 * default method.  If it enters the debugger take the substring(2) of the first arg, and loop
	 * through each character to find the debugging option enabled, if found set the boolean
	 * to true, or set the choice for which solver method to run, then run the program with those 
	 * debugging fields enabled.  If the debugging option is not found, its prints out an invalid option
	 * and exits the debugger.
	 * 
	 * Note that the debugger cannot run more than 1 method at a time, if more than one method
	 * is inserted then it will pick the method closest to the end.
	 * 
	 * @param args
	 * 			
	 */

	public static void main(String[] args) {
		//debugger
		String str = args[0].substring(0, 2).toLowerCase();
		if (str.equals("-o")) {
			str = args[0].substring(2).toLowerCase();
			if (str.equals("options")) {
				System.out
						.println("b: enables a call to isOK after each new board is made.");
				System.out
						.println("h: enables a printout of the HashSet's size after each  board is checked.");
				System.out
						.println("p: enables a printout of each board as it is checked.");
				System.out
						.println("s: enables a printout of the stack or queue size after each board is checked.");
				System.out
						.println("k: enables a printout of each boards blocks as its checked.");
				System.out
						.println("m: enables a printout of the number of moves made to get to the solution.");
				System.out
						.println("c: enables a printout of each boards hashcode as it is checked.");
				System.out.println("1: runs the sparse solver method.");
				System.out.println("2: runs the SparseN solver method.");
				System.out.println("3: runs the Dense solver method.");
				System.out.println("4: runs the Smart solver method.");
				System.out.println("5: runs the Smart_PS solver method.");
				System.out.println("6: runs the SparseNSmart solver method.");
				System.out.println("7: runs the SparseSmart solver method");
				System.exit(0);
			}
			int choice = 0;
			for (int x = 0; x < str.length(); x++) {
				char c = str.charAt(x);
				if (c == 'b') {
					boardIsOK = true;
				} else if (c == 'h') {
					hashSize = true;
				} else if (c == 'p') {
					printBoard = true;
				} else if (c == 's') {
					queueSize = true;
				} else if (c == 'k') {
					printBlocks = true;
				} else if (c == 'm') {
					numberOfMoves = true;
				} else if (c == 'c') {
					hashCode = true;
				} else if (c == '1') {
					choice = 1;
				} else if (c == '2') {
					choice = 2;
				} else if (c == '3') {
					choice = 3;
				} else if (c == '4') {
					choice = 4;
				} else if (c == '5') {
					choice = 5;
				} else if (c == '6') {
					choice = 6;
				} else if (c == '7') {
					choice = 7;
				} else {
					System.out
							.println("The option you chose is not a valid debugging option.");
					System.exit(0);
				}
			}
			Solver s = new Solver(args[1], args[2]);
			s.runGame(choice);
		//end debugger
		} else {//regular method (without -o option)
			Solver s = new Solver(args[0], args[1]);
			s.runGame(0);
		}
	}
}
