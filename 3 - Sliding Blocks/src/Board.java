import java.util.ArrayList;
import java.util.Collections;

public class Board {
	public enum Direction {
		LEFT, RIGHT, UP, DOWN
	};

	private Block[] blocks; // stores block objects (a block contains length,
							// width, row, and col)
	private int height, width; // height and width of the board
	private String moves;
	private Board parentBoard;
	
	/**
	 * Constructor for building an initial board from a file.
	 * 
	 * @param config
	 * 			a file that is read, line by line, where each 
	 * 			line (other than the first) represents a block.
	 */
	public Board(String config) {
		int h, w;
		h = w = 0;
		parentBoard = null;
		Block[] b = new Block[Board.inputLength(config) - 1];
		// Begins building from the initConfig file
		InputSource reader = new InputSource(config);
		String s = reader.readLine();
		if (s != null) {
			h = Integer.parseInt(s.substring(0, s.indexOf(' ')));
			w = Integer.parseInt(s.substring(s.indexOf(' ') + 1));
			s = reader.readLine();
			if (s == null) {
				throw new IllegalArgumentException("There are no blocks!");
			}
			int i = 0;
			while (s != null) {
				b[i] = new Block(s);
				s = reader.readLine();
				i += 1;
			}
		}
		blocks = b;
		height = h;
		width = w;

		if (!isOK()) {
			System.exit(1);
		}
	}

	/** 
	 * Constructor for creating a goal board from an existing
	 * board and a goal config. Not a proper board. Only
	 * contains required goal blocks and no others.
	 * 
	 * @param b
	 * 			another board that has been built already.
	 * 			uses the height and width properties of the board.
	 * 
	 * @param goalConfig
	 * 			a file with block configurations for the goal.
	 */
	public Board(Board b, String goalConfig) {
		parentBoard = null;
		int h = b.height();
		int w = b.width();
		Block[] goalBlocks = new Block[Board.inputLength(goalConfig)];
		InputSource reader = new InputSource(goalConfig);
		String s = reader.readLine();
		if (s == null) {
			throw new IllegalArgumentException("Goal config is empty!");
		}
		int j = 0;
		while (s != null) {
			goalBlocks[j] = new Block(s);
			s = reader.readLine();
			j += 1;
		}
		blocks = goalBlocks;
		height = h;
		width = w;
		if (!isOK()) {
			System.exit(1);
		}
	}

	/** returns the length of a config file for initializing block arrays
	 * 
	 * @param config
	 * 			a text file, where each line represents a block 
	 * 			configuration or the board height and width.
	 * 
	 * @return
	 * 			an int that represents the number of lines in the config file
	 */
	private static int inputLength(String config) {
		InputSource reader = new InputSource(config);
		String s = reader.readLine();
		while (s != null) {
			s = reader.readLine();
		}
		return reader.lineNumber();
	}

	/**
	 * Constructor for building a board from a block array and a set of ints
	 * 
	 * @param b
	 * 			a block array
	 * @param h
	 * 			an int representing board height
	 * @param w
	 * 			an int representing board width
	 */
	public Board(Block[] b, int h, int w) {
		parentBoard = null;
		blocks = b;
		height = h;
		width = w;
		if (!isOK()) {
			System.exit(1);
		}
	}

	/**
	 * Tests if a board is valid.
	 * 
	 * @return
	 * 			a boolean. True if valid. False if invalid.
	 */
	public boolean isOK() {
		try {
			buildTaken();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * a getter method for returning the moves of a board.
	 * 
	 * @return
	 * 			a string moves.
	 */
	public String getMoves() {
		return moves;
	}

	/**
	 * a getter method for returning the barent board of a board
	 * 
	 * @return
	 * 			a board
	 */
	public Board getParentBoard() {
		return parentBoard;
	}


	
	/**
	 * Creates an int by concatenating length, width, row, 
	 * and column of each block. The code is the sum 
	 * of the product of each int and the number of
	 * blocks, % 65537. 65537 was chosen because it 
	 * is the next prime above 256^2, which we felt would
	 * be sufficiently large enough that collisions would be 
	 * greatly reduced.
	 * 
	 * @return
	 * 			a int hashcode.
	 */
//	public int hashCode() {
//		int code = 0;
//		for (Block n : this.blocks) {
//			StringBuffer s = new StringBuffer();
//			s.append(n.length());
//			s.append(n.width());
//			s.append(n.row());
//			s.append(n.column());
//			int x = Integer.parseInt(s.toString());
//			code += (x * blocks.length) % 65537;
//		}
//		return code % 65537;
//	}
	
	public int hashCode() {
		int code = 0;
		for (Block n : this.blocks) {
			String s = "" + n.length() + n.width()+ n.row() + n.column();
			int x = Integer.parseInt(s);
			code += (x * blocks.length) % 65537;
		}
		return code % 65537;
	}

	/**
	 * Compares block in the current board and a
	 * goal board, and returns the distance between
	 * the blocks present in both boards.
	 * 
	 * @param goal
	 * 			the block array of the goal board
	 * 
	 * @return
	 * 			returns an int distance.
	 */
	public int getDistanceToGoal(Block[] goal) {
		int distance = 0;
		for (int i = 0; i < goal.length; i++) {
			ArrayList<Integer> distances = new ArrayList<Integer>();
			for (int j = 0; j < blocks.length; j++) {
				if ((blocks[j].length() == goal[i].length())
						&& (blocks[j].width() == goal[i].width())) {
					int rowDist = Math.abs(blocks[j].row() - goal[i].row());
					int colDist = Math.abs(blocks[j].column()
							- goal[i].column());
					// compute the actual distance between these blocks
					int overallDist = (int) Math.sqrt(rowDist * rowDist
							+ colDist * colDist);
					distances.add(overallDist);
				}
			}
			distance += Collections.min(distances);
		}
		return distance;
	}


	/**
	 *  Sets up an array of taken spots. Does not mark block edges, etc.
	 * 
	 * @return
	 * 			an int array, where -1 represents empty space
	 * 			on a board, and other numbers represent the 
	 * 			the block in that space on the board.
	 */
	public int[][] buildTaken() {
		int[][] taken = new int[height][width];
		// System.out.println("In board, height = "+height+", width = "+width);
		for (int x = 0; x < height; x++) {
			for (int y = 0; y < width; y++) {
				taken[x][y] = -1;
			}
		}

		for (int i = 0; i < blocks.length; i++) {
			for (int row = blocks[i].row(); row < blocks[i].row()
					+ blocks[i].length(); row++) {
				for (int col = blocks[i].column(); col < blocks[i].column()
						+ blocks[i].width(); col++) {
					if (taken[row][col] == -1) {
						taken[row][col] = i;
					} else {
						throw new IllegalStateException("Cannot place blocks.");
					}

				}
			}
		}

		return taken;

	}

	/**
	 * getter method for the board width
	 * 
	 * @return
	 * 			an int (the width of the board)
	 */
	public int width() {
		return width;
	}
	
	/**
	 * 	getter method for the board height
	 * 
	 * @return
	 * 			an int (the height of the board)
	 */
	public int height() {
		return height;
	}

	/**
	 * getter method for the blocks of the board.
	 * 
	 * @return
	 * 			a block array
	 */
	public Block[] blocks() {
		return blocks;
	}

	/**
	 * Returns the index of the block (in the blocks 
	 * array) at the given location. Only checks upper 
	 * left corner of blocks.
	 * 
	 * @param row
	 * 			the board row
	 * @param column
	 * 			the board column
	 * @return
	 * 			an int representing the index of the 
	 * 			block at the given position. Returns -1
	 * 			the space is empty.
	 */
	public int getBlockAt(int row, int column) {
		for (int i = 0; i < blocks.length; i++) {
			if ((row == blocks[i].row()) && (column == blocks[i].column())) {
				return i;
			}
		}
		return -1;
	}

	 /**
	  * Determines if a move is valid by checking
	  * if a block is currently in the new position, or
	  * if the new position is still on the board. Invalid
	  * moves include moving blocks where to currently
	  * occupied spaces, and moving block off the board.
	  * 
	  * @param blRow
	  * 			the current row of the block
	  * @param blCol
	  * 			the current column of the block
	  * @param newRow
	  * 			the row where the block is to be moved
	  * @param newCol
	  * 			the column where the block is to be moved
	  * 
	  * @return
	  * 			a boolean, true if the move is valie, false if invalid.
	  */
	public boolean canMove(int blRow, int blCol, int newRow, int newCol) {
		int index = getBlockAt(blRow, blCol);
		int[][] taken = buildTaken();
		boolean validMove = true;
		// for loop below doesn't work if newRow,newCol are negative so need to
		// check here
		if (!isInBounds(newRow, newCol)) {
			return false;
		}
		// only works if moving one space at a time...
		for (int row = newRow; row < newRow + blocks[index].length(); row++) {
			for (int col = newCol; col < newCol + blocks[index].width(); col++) {
				if (!isInBounds(row, col)
						|| !(taken[row][col] == -1 || taken[row][col] == index)) {
					validMove = false;
				}
			}
		}

		return validMove;
	}

	/**
	 * 	Sets up an array of taken spots. Does not mark block edges, etc.
	 * 
	 * @return
	 * 			a boolean array representing the spaces
	 * 			occupied on the board by blocks. True if 
	 * 			occupird, false if empty.
	 */
	public boolean[][] buildBooleanTaken() {
		boolean[][] taken = new boolean[height][width];

		for (int i = 0; i < blocks.length; i++) {
			for (int row = blocks[i].row(); row < blocks[i].row()
					+ blocks[i].length(); row++) {
				for (int col = blocks[i].column(); col < blocks[i].column()
						+ blocks[i].width(); col++) {
					taken[row][col] = true;
				}
			}
		}
		return taken;
	}

	/**
	 * Checks if a move is valid.
	 * 
	 * @param row
	 * 			the row of the current block.
	 * @param column
	 * 			the column of the current block.
	 * @param dir
	 * 			the direction to be moved.
	 * @return
	 * 			a boolean. true if the move is valid, false if invalid.
	 */
	public boolean canMove(int row, int column, Direction dir) {
		int index = getBlockAt(row, column);
		if (index == -1)
			return false; // cannot move nonexistant block
		return canMove(index, dir);
	}

	/**
	 * 	Original canMove, takes a direction and checks if block can move
	 * 
	 * @param index
	 * 			the index of the block to be checked in the block array.
	 * @param dir
	 * 			the direction the block is to be moved
	 * @return
	 * 			a boolean. True if the move is valid, false if invalid.
	 */
	public boolean canMove(int index, Direction dir) {
		int row = blocks[index].row();
		int column = blocks[index].column();

		boolean[][] Taken = buildBooleanTaken();

		try {
			if (dir == Direction.UP) {
				for (int i = 0; i < blocks[index].width(); i++) {
					if (Taken[row - 1][column + i])
						return false;
				}
			}
			if (dir == Direction.DOWN) {
				for (int i = 0; i < blocks[index].width(); i++) {
					if (Taken[row + blocks[index].length()][column + i])
						return false;
				}
			}
			if (dir == Direction.LEFT) {
				for (int i = 0; i < blocks[index].length(); i++) {
					if (Taken[row + i][column - 1])
						return false;
				}
			}
			if (dir == Direction.RIGHT) {
				for (int i = 0; i < blocks[index].length(); i++) {
					if (Taken[row + i][column + blocks[index].width()])
						return false;
				}
			}
		} catch (Exception e) { // index out of bounds
			return false;
		}

		return true;
	}

	/**
	 * Original move method, takes a direction and moves the block by one square
	 * 
	 * @param index
	 * 			index of the current block in the board's block array
	 * @param dir
	 * 			the direction the block is to be moved
	 * @return
	 * 			a boolean, true is the move is valid, false if invalid.
	 */
	public Board move(int index, Direction dir) {
		int row = blocks[index].row();
		int column = blocks[index].column();

		Block[] newBlocks = new Block[blocks.length];

		String s = row + " " + column;
		if (dir == Direction.UP)
			row += -1;
		if (dir == Direction.DOWN)
			row += 1;
		if (dir == Direction.LEFT)
			column += -1;
		if (dir == Direction.RIGHT)
			column += 1;

		s = s + " " + row + " " + column;

		for (int i = 0; i < blocks.length; i++) {
			newBlocks[i] = blocks[i];
		}
		newBlocks[index] = new Block(blocks[index]);
		newBlocks[index].setRow(row);
		newBlocks[index].setColumn(column);
		Board newBoard = new Board(newBlocks, height, width);
		newBoard.parentBoard = this;
		newBoard.moves = s;
		return newBoard;
	}

	/**
	 * The move method. Moves a block on the board.
	 * 
	 * @param row
	 * 			the row of the current block to be moved.
	 * @param column
	 * 			the row of the current block to be moved.
	 * @param dir
	 * 			the direction the block is to be moved.
	 * @return
	 * 			a board with the new block locations after
	 * 			the move has been made.
	 */
	public Board move(int row, int column, Direction dir) {
		int index = getBlockAt(row, column);
		return move(index, dir);
	}

	public boolean isInBounds(int row, int col) {
		if (row < height && col < width && row >= 0 && col >= 0) {
			return true;
		}
		return false;
	}

	/**
	 * 	 Moves a block one space in the specified direction, 
	 * updates Taken and blocks.
	 * 
	 * @param blRow
	 * 			the row of the block to be moved.
	 * @param blCol
	 * 			the column of the block to be moved.
	 * @param newRow
	 * 			the row where the block is to be moved.
	 * @param newCol
	 * 			the column where the block is to be moved.
	 * @return
	 * 			a board with the new block locations after
	 * 			the move has been made.
	 */
	public Board move(int blRow, int blCol, int newRow, int newCol) {
		if (canMove(blRow, blCol, newRow, newCol)) {
			int index = getBlockAt(blRow, blCol);
			Block [ ] newBlocks = new Block [blocks.length];
			for(int i = 0; i < blocks.length; i++)
			{
				newBlocks[i] = new Block(blocks[i]);
			}
			Board b = new Board(newBlocks, this.height(), this.width());
			b.blocks[index].setColumn(newCol);
			b.blocks[index].setRow(newRow);
			b.parentBoard = this;
			b.moves = blRow + " " + blCol + " " + newRow + " " + newCol;
			return b;
		}
		return null;
	}

	/**
	 * 	generates an ArrayList of boards who have 
	 * valid moves given the current board
	 * 
	 * @return
	 * 			An arraylist that contains all the valid boards 
	 * 			that could result from all the valid moves 
	 * 			a board could ever make, based on the
	 * 			position of the white spaces.
	 */
	public ArrayList<Board> generateMoveList() {
		ArrayList<Board> boardsList = new ArrayList<Board>();
		int[][] taken = buildTaken();
		// printTaken();

		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				if (taken[row][col] == -1) {
					ArrayList<Integer> blockIndecies = grabIndicies(row, col,
							taken);
					for (int i : blockIndecies) {
						if (i >= 0) {
							int r = blocks[i].row();
							int c = blocks[i].column();
							Board newBoard1 = move(r, c, r - 1, c);
							if (newBoard1 != null
									&& !boardsList.contains(newBoard1)) {
								boardsList.add(newBoard1);
							}
							Board newBoard2 = move(r, c, r, c - 1);
							if (newBoard2 != null
									&& !boardsList.contains(newBoard2)) {
								boardsList.add(newBoard2);
							}
							Board newBoard3 = move(r, c, r + 1, c);
							if (newBoard3 != null
									&& !boardsList.contains(newBoard3)) {
								boardsList.add(newBoard3);
							}
							Board newBoard4 = move(r, c, r, c + 1);
							if (newBoard4 != null
									&& !boardsList.contains(newBoard4)) {
								boardsList.add(newBoard4);
							}
						}
					}
				}
			}
		}
		return boardsList;
	}

	/**
	 * 	Given a row and column, this method will grab all
	 * the positions to the top, left, right, and bottom
	 * of that initial row and column.
	 * 
	 * @param row
	 * 			the current row
	 * @param col
	 * 			the current column
	 * @param taken
	 * 			an int array of spaces taken in the board.
	 * @return
	 * 			an arraylist of the integer positions in the board.
	 */
	public ArrayList<Integer> grabIndicies(int row, int col, int[][] taken) {
		ArrayList<Integer> indicies = new ArrayList<Integer>();
		int up, down, left, right;
		if (isInBounds(row + 1, col)) {
			down = taken[row + 1][col];
			indicies.add(down);
		}
		if (isInBounds(row, col + 1)) {
			right = taken[row][col + 1];
			indicies.add(right);
		}
		if (isInBounds(row - 1, col)) {
			up = taken[row - 1][col];
			indicies.add(up);
		}
		if (isInBounds(row, col - 1)) {
			left = taken[row][col - 1];
			indicies.add(left);
		}
		return indicies;
	}
	/**
	 * Compares the current board to the goalBlocks board.
	 * Compares the block arrays of the two boards, and
	 * checks that all the blocks of the goalBoard are present
	 * in the current board.
	 * 
	 * @param goalBlocks
	 * 			a block array of the goal board.
	 * @return
	 * 			a boolean. true if all the goal blocks are present 
	 * 			in the current board, false otherwise.
	 */

	public boolean checkBoard(Block[] goalBlocks) {
		for (Block i : goalBlocks) {
			if (!contains(blocks, i)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Looks for a block in a block array.
	 * 
	 * @param list
	 * 			a block array
	 * @param b
	 * 			the block to be searched for
	 * @return
	 * 			a boolean. true if the block is present, false otherwise.
	 */

	public boolean contains(Block[] list, Block b) {
		for (Block i : list) {
			if (i.equals(b)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Compares board objects.
	 * Boards are equal if their block, height, 
	 * and width are equal.
	 * 
	 * @param obj
	 * 			a second board.
	 * @return
	 * 			a boolean. True if the boards are equal, false otherwise.
	 */

	// override .equals() method
	public boolean equals(Object obj) {
		Board b = (Board) obj;
		if (b.height == height && b.width == width && checkBoard(b.blocks)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Loops through the blocks of a board and prints
	 * out the configurations of the blocks.
	 */

	// Displays the list of blocks and their positions.
	public void printBlocks() {
		System.out.println("#\tLength\tWidth\tRow\tColumn");
		for (int i = 0; i < blocks.length; i++) {
			System.out.print(i + ".\t" + blocks[i].length() + "\t"
					+ blocks[i].width() + "\t");
			System.out.print(blocks[i].row() + "\t" + blocks[i].column());
			System.out.println();
		}
	}
	
	/** 
	 * Loops through the blocks of a board and prints
	 * out the board configuration, with indices of the
	 * blocks, and -1 representing white space.
	 */

	// Shows which spots on the board are occupied by some block.
	public void printTaken() {
		int[][] taken = buildTaken();
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				System.out.print(taken[i][j] + "\t");
			}
			System.out.println();
		}
	}

}