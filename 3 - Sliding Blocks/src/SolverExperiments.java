import junit.framework.TestCase;

public class SolverExperiments extends TestCase {

	public int testNum = 5;
	
	public void testDads() {
		
		Solver s = new Solver("medium/dads", "medium/dads.goal");
		s.runGame(testNum);

	}
	public void testDads_180() {

		Solver s = new Solver("medium/dads+180", "medium/dads+180.goal");
		s.runGame(testNum);

	}
	public void testBlockado() {

		Solver s = new Solver("medium/blockado", "medium/blockado.goal");
		s.runGame(testNum);

	}
	public void testC28() {

		Solver s = new Solver("medium/c28", "medium/28.goal");
		s.runGame(testNum);

	}
	public void testC29() {

		Solver s = new Solver("medium/c29", "medium/29.goal");
		s.runGame(testNum);

	}
	public void testC31() {

		Solver s = new Solver("medium/c31", "medium/31.goal");
		s.runGame(testNum);

	}
	public void testC32() {

		Solver s = new Solver("medium/c32", "medium/32.goal");
		s.runGame(testNum);

	}
	public void testC33() {

		Solver s = new Solver("medium/c33", "medium/33.goal");
		s.runGame(testNum);

	}
	public void testC34() {

		Solver s = new Solver("medium/c34", "medium/34.goal");
		s.runGame(testNum);

	}
	public void testC35() {

		Solver s = new Solver("medium/c35", "medium/35.goal");
		s.runGame(testNum);

	}
	public void testC36() {

		Solver s = new Solver("medium/c36", "medium/36.goal");
		s.runGame(testNum);

	}
	public void testC37() {

		Solver s = new Solver("medium/c37", "medium/37.goal");
		s.runGame(testNum);

	}
	public void testC38() {

		Solver s = new Solver("medium/c38", "medium/38.goal");
		s.runGame(testNum);

	}
	
}
