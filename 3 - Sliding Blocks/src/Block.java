public class Block
{
	private int length, width, row, column;

	/**
	 * 	Constructor for a block.
	 * 
	 * @param params
	 * 			an int array of 4 values: length, width, row, and column.
	 */
	public Block(int[] params) {
		length = params[0];
		width = params[1];
		row = params[2];
		column = params[3];
	}

	/**
	 * Constructor for a block. Copies another block.
	 * 
	 * @param b
	 * 			the other block
	 */
	public Block(Block b) {
		length = b.length;
		width = b.width;
		row = b.row;
		column = b.column;
	}

	/**
	 * Constructor for a block. Creates a block from a string.
	 * 
	 * @param params
	 * 			a string of 4 ints:
	 * 				length
	 * 				width
	 * 				row
	 * 				column
	 */
	public Block(String params) {
		String[] s = params.split(" ",4);
		length = Integer.parseInt(s[0]);
		width = Integer.parseInt(s[1]);
		row = Integer.parseInt(s[2]);
		column = Integer.parseInt(s[3]);
	}

	/**
	 * Compares two blocks. Equal blocks have the 
	 * same length and width, as well as the same 
	 * row and column positions.
	 * 
	 * @param b
	 * 			the other block to compare to.
	 * @return
	 * 			a boolean. True if equal, false otherwise.
	 */
	public boolean equals (Block b)
	{
		if(column == b.column
				&& row == b.row
				&& width == b.width
				&& length == b.length)
		{
			return true;
		}
		return false;
	}
	
	/**
	 * Getter method the block length.
	 * @return
	 * 			an int, the length.
	 */
	public int length() { return length; }
	
	/**
	 * Getter method the block width.
	 * @return
	 * 			an int, the width.
	 */
	public int width() { return width; }
	
	/**
	 * Getter method the block row.
	 * @return
	 * 			an int, the row.
	 */
	public int row() { return row; }
	
	/**
	 * Setter method the block column.
	 */
	public void setRow(int r) { row = r; }
	
	/**
	 * Getter method the block column.
	 * @return
	 * 			an int, the column.
	 */
	public int column() { return column; }
	
	/**
	 * Setter method the block length.
	 */
	public void setColumn(int c) { column = c; }
}