import junit.framework.TestCase;

public class BoardTest extends TestCase {
	
	// hashcode edge case - same blocks in different board positions
	public void testHash1() {

	    Block [] blocks = new Block[2];
	    blocks[0] = new Block("1 1 0 0");
	    blocks[1] = new Block("1 1 1 1");
	    Board newBoard = new Board(blocks, 4, 5);
	    Block [] blocks2 = new Block[2];
	    blocks2[0] = new Block("1 1 0 0");
	    blocks2[1] = new Block("1 1 1 0");
	    Board newBoard2 = new Board(blocks2, 4, 5);
	    assertFalse(newBoard.hashCode() == newBoard2.hashCode());
	}
    
	// hashcode edge case - same blocks in different order
    public void testHash2() {

	    Block [] blocks = new Block[2];
	    blocks[0] = new Block("1 1 1 1");
	    blocks[1] = new Block("1 1 0 0");
	    Board newBoard = new Board(blocks, 4, 5);
	    Block [] blocks2 = new Block[2];
	    blocks2[0] = new Block("1 1 0 0");
	    blocks2[1] = new Block("1 1 1 1");
	    Board newBoard2 = new Board(blocks2, 4, 5);
	    assertTrue(newBoard.hashCode() == newBoard2.hashCode());
    }
    
	// hashcode edge case - same blocks in different order - larger board
    public void testHash3() {
	    Block [] blocks = new Block[2];
	    blocks[0] = new Block("4 4 0 0");
	    blocks[1] = new Block("4 4 250 250");
	    Board newBoard = new Board(blocks, 256, 256);
	    Block [] blocks2 = new Block[2];
	    blocks2[0] = new Block("4 4 250 250");
	    blocks2[1] = new Block("4 4 0 0");
	    Board newBoard2 = new Board(blocks2, 256, 256);
	    assertTrue(newBoard.hashCode() == newBoard2.hashCode());
    }
    
	// hashcode edge case - same blocks in mirrored positions
    public void testHash4() {

        Block [] blocks = new Block[2];
        blocks[0] = new Block("1 1 0 0");
        blocks[1] = new Block("1 1 255 255");
        Board newBoard = new Board(blocks, 256, 256);
        Block [] blocks2 = new Block[2];
        blocks2[0] = new Block("1 1 255 0");
        blocks2[1] = new Block("1 1 0 255");
        Board newBoard2 = new Board(blocks2, 256, 256);
        assertFalse(newBoard.hashCode() == newBoard2.hashCode());
    }
    
	// hashcode edge case - same blocks in mirrored positions
    public void testHash5() {

        Block [] blocks = new Block[2];
        blocks[0] = new Block("1 1 0 255");
        blocks[1] = new Block("1 1 255 255");
        Board newBoard = new Board(blocks, 256, 256);
        Block [] blocks2 = new Block[2];
        blocks2[0] = new Block("1 1 0 0");
        blocks2[1] = new Block("1 1 255 0");
        Board newBoard2 = new Board(blocks2, 256, 256);
        assertFalse(newBoard.hashCode() == newBoard2.hashCode());
    }
    
    // tests board constructor from block array
    public void testConstructor1() {
        Block [] blocks = new Block[10];
        blocks[0] = new Block("2 1 0 0");
        blocks[1] = new Block("2 1 0 3");
        blocks[2] = new Block("2 1 2 0");
        blocks[3] = new Block("2 1 2 3");
        blocks[4] = new Block("2 2 1 1");
        blocks[5] = new Block("1 2 3 1");
        blocks[6] = new Block("1 1 4 0");
        blocks[7] = new Block("1 1 4 1");
        blocks[8] = new Block("1 1 4 2");
        blocks[9] = new Block("1 1 4 3");
        Board newBoard1 = new Board(blocks, 256, 256);
        assertTrue(newBoard1 != null);
    }
    
    // tests board constructor from file
    public void testConstructor2() {
    	Board newBoard1 = new Board("easy/enormous.full.1");
    	Board newBoard2 = new Board("hard/big.tray.4");
    	Board newBoard3 = new Board("hard/c22");
    	Board newBoard4 = new Board("hard/c25");
    	Board newBoard5 = new Board("hard/c26");
    	Board newBoard6 = new Board("hard/c43-45");
    	Board newBoard7 = new Board("hard/c46");
    	Board newBoard8 = new Board("hard/c53");
    	Board newBoard9 = new Board("hard/c54");
    	Board newBoard10 = new Board("hard/c55");
    	Board newBoard11 = new Board("hard/c64");
    	Board newBoard12 = new Board("hard/c71");
    	Board newBoard13 = new Board("hard/century+180");
    	Board newBoard14 = new Board("hard/big.tray.3");
    	Board newBoard15 = new Board("hard/big.tray.1");
    	Board newBoard16 = new Board("easy/140x140");
    	assertTrue(newBoard1 != null &&
    				newBoard2 != null &&
    				newBoard3 != null &&
    				newBoard4 != null &&
    				newBoard5 != null &&
    				newBoard6 != null &&
    				newBoard7 != null &&
    				newBoard8 != null &&
    				newBoard9 != null &&
    				newBoard10 != null &&
    				newBoard11 != null &&
    				newBoard12 != null &&
    				newBoard13 != null &&
    				newBoard14 != null &&
    				newBoard15 != null &&
    				newBoard16 != null);
    }
    
    // tests the checkboard method for finding a solution
    public void testCheckBoard() {
    	Block [] newBlocks = new Block[7], goalBlocks = new Block[7];
    	newBlocks[0] = new Block("2 2 0 0");
    	newBlocks[1] = new Block("1 1 0 2");
    	newBlocks[2] = new Block("1 1 1 2");
    	newBlocks[3] = new Block("1 1 2 2");
    	newBlocks[4] = new Block("1 1 2 0");
    	newBlocks[5] = new Block("1 1 2 1");
    	newBlocks[6] = new Block("2 2 4 1");
    	goalBlocks[0] = new Block("2 2 0 0");
    	goalBlocks[1] = new Block("1 1 0 2");
    	goalBlocks[2] = new Block("1 1 1 2");
    	goalBlocks[3] = new Block("1 1 2 2");
    	goalBlocks[4] = new Block("1 1 2 0");
    	goalBlocks[5] = new Block("1 1 2 1");
    	goalBlocks[6] = new Block("2 2 4 0");
    	Board newBoard = new Board(newBlocks, 6, 6);
    	Board goalBoard = new Board(goalBlocks, 6, 6);
    	Board newBoard2 = newBoard.move(6, Board.Direction.LEFT);
    	assertTrue(newBoard2.checkBoard(goalBoard.blocks()));
    }

    // tests the checkboard method for just the goal blocks
    public void testCheckBoard2() {
    	Block [] newBlocks = new Block[5], goalBlocks = new Block[2];
    	newBlocks[0] = new Block("2 2 0 0");
    	newBlocks[1] = new Block("1 1 0 2");
    	newBlocks[2] = new Block("1 1 1 2");
    	newBlocks[3] = new Block("1 1 2 2");
    	newBlocks[4] = new Block("2 2 4 1");
    	goalBlocks[0] = new Block("2 2 4 2");
    	goalBlocks[1] = new Block("2 2 4 0");
    	Board newBoard = new Board(newBlocks, 6, 6);
    	Board goalBoard = new Board(goalBlocks, 6, 6);
    	Board newBoard2 = newBoard.move(4, Board.Direction.RIGHT);
    	Board newBoard3 = newBoard2.move(0, Board.Direction.DOWN);
    	Board newBoard4 = newBoard3.move(0, Board.Direction.DOWN);
    	Board newBoard5 = newBoard4.move(0, Board.Direction.DOWN);
    	Board newBoard6 = newBoard5.move(0, Board.Direction.DOWN);
    	assertTrue(newBoard6.checkBoard(goalBoard.blocks()));
    }
    
    // tests board constructor for goal boards
    public void testConstructor3() {
    	Board newBoard1 = new Board("easy/big.block.1");
    	Board goalBoard = new Board(newBoard1, "easy/big.block.1.goal");
    	Board newBoard2 = newBoard1.move(3, Board.Direction.UP);
    	Board newBoard3 = newBoard2.move(3, Board.Direction.LEFT);
    	Board newBoard4 = newBoard3.move(3, Board.Direction.LEFT);
    	assertTrue(newBoard4.checkBoard(goalBoard.blocks()));    	
    }

    // should produce collision
    // Test is purposefully designed to produce a collision
    // by using 9 boards with a single block, with height, width,
    // row and column values that produce the same int once 
    // concatenated together. Each of the nine boards will produce
    // same hashcode.
    public void testCollision1() {
    	Block[] ab = new Block[1], bb = new Block[1], cb = new Block[1],
    	db = new Block[1], eb = new Block[1], fb = new Block[1],
    	gb = new Block[1], hb = new Block[1], ib = new Block[1];
    	ab[0] = new Block("1 111 111 1");
    	bb[0] = new Block("1 111 11 11");
    	cb[0] = new Block("1 111 1 111");
    	db[0] = new Block("11 11 111 1");
    	eb[0] = new Block("11 11 11 11");
    	fb[0] = new Block("11 11 1 111");
    	gb[0] = new Block("111 1 111 1");
    	hb[0] = new Block("111 1 11 11");
    	ib[0] = new Block("111 1 1 111");
    	Board r = new Board(ab, 256, 256), s = new Board(bb, 256, 256),
    	t = new Board(cb, 256, 256), u = new Board(db, 256, 256), 
    	v = new Board(eb, 256, 256), w = new Board(fb, 256, 256), 
    	x = new Board(gb, 256, 256), y = new Board(hb, 256, 256),
    	z = new Board(ib, 256, 256);
    	assertTrue(r.hashCode() == s.hashCode() &&
    		s.hashCode() == t.hashCode() &&
    		t.hashCode() == u.hashCode() &&
    		u.hashCode() == v.hashCode() &&
    		v.hashCode() == w.hashCode() &&
    		w.hashCode() == x.hashCode() &&
    		x.hashCode() == y.hashCode() &&
    		y.hashCode() == z.hashCode());
    }
}
