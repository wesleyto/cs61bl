public class BoardNode implements Comparable {
    Board myBoard;
    int priority;
    
    /**
     * Constructor for a BoardNode
     * 
     * @param b
     * 			a board
     * @param p
     * 			an int representing the priority.
     * 			a lower value is a higher priority.
     */
    public BoardNode(Board b, int p) {
        myBoard = b;
        priority = p;
    }
    
    /**
     * Compares the priority of a Board to the priority of another board.
     * 
     * @param o
     * 			another board
     * 
     * @return
     * 			an int. 
     * 			0 if the two priorities are equal.
     * 			Negative if the current board is less than o.
     * 			Positive otherwise.
     */
    public int compareTo(Object o) {
        return ((Integer)priority).compareTo((Integer)((BoardNode)o).priority);
    }

}
