import junit.framework.TestCase;

public class SolverExperiments2 extends TestCase {

		public int testNum = 5;
	// hard tests
		public void testSuperCentury() {

			Solver s = new Solver("hard/super.century", "hard/super.century.goal");
			s.runGame(testNum);

		}	
		public void testPennant() {

			Solver s = new Solver("hard/pennant", "hard/pennant.goal");
			s.runGame(testNum);

		}
		public void testLaneRouge() {

			Solver s = new Solver("hard/lane.rouge", "hard/lane.rouge.goal");
			s.runGame(testNum);

		}
		public void testSuperCentury2() {

			Solver s = new Solver("hard/super.century2", "hard/super.century2.goal");
			s.runGame(testNum);

		}
		
	/*	public void testPandemonium() {

			Solver s = new Solver("hard/pandemonium", "hard/pandemonium.goal");
			s.runGame(testNum);

		}*/
		
		public void testC22() {

			Solver s = new Solver("hard/c22", "hard/22.goal");
			s.runGame(testNum);

		}	
		public void testC25() {

			Solver s = new Solver("hard/c25", "hard/15.23-27.30.41.goal");
			s.runGame(testNum);

		}	
		public void testC26() {

			Solver s = new Solver("hard/c26", "hard/15.23-27.30.41.goal");
			s.runGame(testNum);

		}	
		public void testC46() {

			Solver s = new Solver("hard/c46", "hard/46.goal");
			s.runGame(testNum);

		}	
		public void testC53() {

			Solver s = new Solver("hard/c53", "hard/53.54.64.goal");
			s.runGame(testNum);

		}	
		public void testC54() {

			Solver s = new Solver("hard/c54", "hard/53.54.64.goal");
			s.runGame(testNum);

		}	
		public void testC55() {

			Solver s = new Solver("hard/c55", "hard/55.56.goal");
			s.runGame(testNum);

		}	
		
	
}
